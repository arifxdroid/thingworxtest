package com.ideologyllc.thingworxtest;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

//import com.example.androidexamplething.R;
import com.thingworx.communications.client.things.VirtualThing;
import com.thingworx.communications.client.things.VirtualThingPropertyChangeEvent;
import com.thingworx.communications.client.things.VirtualThingPropertyChangeListener;
import com.thingworx.types.primitives.NumberPrimitive;

import java.text.DecimalFormat;


/**
 * This class creates an Activity that manages one or more VirtualThings. It gets all of its
 * ThingWorx specific features from its base class and focuses on the creation of your
 * virtual things, in this case, a single Steam Sensor and the creation of the UI. It also binds
 * the values of the Steam Sensor VirtualThing directly to the controls in this activity. It also
 * provides a generic settings UI to configure the ThingWorx client connection.
 */
public class MainActivity extends ThingworxActivity {

    private final static String logTag = MainActivity.class.getSimpleName();
    public static final int POLLING_RATE = 1000;

    private final String TAG = MainActivity.class.getName();

    private ProgressBar temperatureProgressBar;
    private ProgressBar pressureProgressBar;
    private ProgressBar totalFlowProgressBar;
    private TextView textViewTemperature;
    private TextView textViewPressure;
    private TextView textViewTotalFlow;
    private TextView textViewTemperatureLimit;
    private SeekBar seekBarTemperatureLimit;
    private SteamThing sensor1;
    private CheckBox faultStatusCheckBox;
    private CheckBox checkBoxConnected;
    private CheckBox toggleButtonInletValve;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        // Build User Interface
        setContentView(R.layout.activity_main);
        setTitle("Android Steam Thing");

        // Get reference to controls
        temperatureProgressBar = (ProgressBar) findViewById(R.id.temperatureProgressBar);
        temperatureProgressBar.setMax(40);
        pressureProgressBar = (ProgressBar) findViewById(R.id.pressureProgressBar);
        pressureProgressBar.setMax(5);
        totalFlowProgressBar = (ProgressBar) findViewById(R.id.totalFlowProgressBar);
        totalFlowProgressBar.setMax(100);
        textViewTemperature = (TextView) findViewById(R.id.textViewTemperature);
        textViewPressure = (TextView) findViewById(R.id.textViewPressure);
        textViewTotalFlow = (TextView) findViewById(R.id.textViewTotalFlow);
        textViewTemperatureLimit = (TextView) findViewById(R.id.textViewTemperatureLimit);
        seekBarTemperatureLimit = (SeekBar) findViewById(R.id.seekBarTemperatureLimit);
        seekBarTemperatureLimit.setMax(500);
        faultStatusCheckBox = (CheckBox) findViewById(R.id.faultStatusCheckBox);
        checkBoxConnected = (CheckBox) findViewById(R.id.checkBoxConnected);
        toggleButtonInletValve = (CheckBox) findViewById(R.id.toggleButtonInletValve);

        // Support slider changes
        seekBarTemperatureLimit.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                try {
                    sensor1.setProperty("TemperatureLimit", new NumberPrimitive(progress));
                } catch (Exception e) {
                    Log.d(TAG, "Failed to set TemperatureLimit in response to slider change.");
                }
            }
            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        // Create your Virtual Thing and bind it to your android controls
        try {

            sensor1 = new SteamThing("SteamSensor1", "1st Floor Steam Sensor", client);

            /* Adding a property change listener to your VirtualThing is a convenient way to directly
             * bind your android controls to property values. They will get updated
             * as soon as they are changed, either on the server or locally                       */
            sensor1.addPropertyChangeListener(new VirtualThingPropertyChangeListener() {
                @Override
                public void propertyChangeEventReceived(final VirtualThingPropertyChangeEvent evt) {
                    final String propertyName = evt.getProperty().getPropertyDefinition().getName();
                    runOnUiThread(new Runnable() { // Always update your controls on the UI thread
                        @Override
                        public void run() {
                            // change UI elements here
                            DecimalFormat df = new DecimalFormat("#.##");
                            if (propertyName.equals("Temperature")) {
                                Double temperature = (Double) evt.getPrimitiveValue().getValue();
                                temperatureProgressBar.setProgress(temperature.intValue() - 400);
                                textViewTemperature.setText(df.format(temperature) + " F");
                            } else if (propertyName.equals("Pressure")) {
                                Double pressure = (Double) evt.getPrimitiveValue().getValue();
                                pressureProgressBar.setProgress(pressure.intValue() - 18);
                                textViewPressure.setText(df.format(pressure) + " Psi");
                            } else if (propertyName.equals("TotalFlow")) {
                                Double totalFlow = (Double) evt.getPrimitiveValue().getValue();
                                totalFlowProgressBar.setProgress(totalFlow.intValue());
                                textViewTotalFlow.setText(df.format(totalFlow) + " lb");
                            } else if (propertyName.equals("TemperatureLimit")) {
                                Double tempLimit = (Double) evt.getPrimitiveValue().getValue();
                                seekBarTemperatureLimit.setProgress(tempLimit.intValue());
                                textViewTemperatureLimit.setText(df.format(tempLimit) + " F");
                            } else if (propertyName.equals("FaultStatus")) {
                                Boolean falutStatus = (Boolean) evt.getPrimitiveValue().getValue();
                                faultStatusCheckBox.setChecked(falutStatus);
                            } else if (propertyName.equals("InletValve")) {
                                Boolean isInletValve = (Boolean) evt.getPrimitiveValue().getValue();
                                toggleButtonInletValve.setChecked(isInletValve);
                            }
                        }
                    });
                }
            });

            // You only need to do this once, no matter how many things your add
            startProcessScanRequestThread(POLLING_RATE, new ConnectionStateObserver() {
                @Override
                public void onConnectionStateChanged(final boolean connected) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            checkBoxConnected.setChecked(connected);
                        }
                    });
                }
            });

            // If you don't have preferences, display the dialog to get them.
            if (!hasConnectionPreferences()) {
                // Show Preferences Activity
                connectionState = ConnectionState.DISCONNECTED;
                Intent i = new Intent(this, PreferenceActivity.class);
                startActivityForResult(i, 1);
                return;
            }


            connect(new VirtualThing[]{sensor1});

        } catch (Exception e) {
            Log.e(TAG, "Failed to initalize with error.", e);
            onConnectionFailed("Failed to initalize with error : " + e.getMessage());
        }

    }

    /**
     * Resume will be called each time this activity becomes active.
     * Check your connection state and try to establish a connection.
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume() called.");
        if(getConnectionState() == ConnectionState.DISCONNECTED) {
            try {
                connect(new VirtualThing[]{sensor1});
            } catch (Exception e) {
                Log.e(TAG, "Restart with new settings failed.", e);
            }
        }
    }

    /**
     * This function will be called from the base class to allow you to set
     * values on your virtual thing that are not configured in your aspect defaults or to perform
     * any other UI changes in response to becomming connected to the server.
     */
    @Override
    protected void onConnectionEstablished() {
        super.onConnectionEstablished();
        try {
            sensor1.setTemperatureLimit(430.0);
        } catch (Exception e) {
            Log.w(TAG, "Failed to set initial temperature limit.");
        }
    }

    /**** Support for Settings Menu ****/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_settings was selected
            case R.id.action_settings:
                disconnect();
                Intent i = new Intent(this, PreferenceActivity.class);
                startActivityForResult(i, 1);
                break;
            default:
                break;
        }
        return true;
    }


}